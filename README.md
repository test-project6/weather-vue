# Vue Js

This is a step-by-step guide to setup weather application

## Install npm

```bash
npm install
```

## Update domain of Laravel Backend Api in .env file

1. Find .env in project root.
2. Change variable of VUE_APP_ROOT_URL to your preferred domain.

## Serve your application

```bash
npm run serve
```

## Instruction to use the application

1. You are able to see the page with only user input once you open the application.
2. Either select from dropdown or enter desired city name on input field.
3. Click "ADD" button to add into the list.
4. You can add up to 20 cities.
5. You can remove particular city by hovering desired box and click "REMOVE" button.
6. Each city will refresh its data for every minute.

