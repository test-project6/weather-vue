import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export const urlState = process.env.VUE_APP_ROOT_URL ? process.env.VUE_APP_ROOT_URL : 'localhost/8000';

export default new Vuex.Store({
  state: {
    cities: [],
    cityList: [],
    isLoading: false,
    fullPage: true
  },

  getters: {
    countCities(state) {
        return state.cities.length;
    }
  },

  mutations: {
    allInfo(state, payload) {
      state.cities = payload;
    },
    allList(state, payload) {
      state.cityList = payload;
    },
    setLoadingPage(state, payload) {
        state.isLoading = payload;
    }
  },

  actions: {
    async getCity(state) {
      state.commit('setLoadingPage', true);
      axios.get(urlState + '/api/city')
        .then(response => {
          state.commit('allInfo', response.data);
          setTimeout(() => state.commit('setLoadingPage', false), 800);
        })
        .catch(error => {
          console.log(error);
        })
    },
    async removeCity(state, id) {
      axios.delete(urlState + '/api/city/delete/' + id)
        .then(response => {
          state.commit('allInfo', response.data)
        })
        .catch(error => {
          console.log(error)
        })
    },
    async syncCity(state, ids) {
      state.commit('setLoadingPage', true);
      axios.post(urlState + '/api/city/sync')
          .then(response => {
            state.commit('allInfo', response.data);
            setTimeout(() => state.commit('setLoadingPage', false), 800);
          })
          .catch(error => {
            console.log(error)
          })
    },
    async getCityList(state) {
      axios.get(urlState + '/api/city/list')
          .then(response => {
            state.commit('allList', response.data)
          })
          .catch(error => {
            console.log(error)
          })
    },
    async addCity(state, value) {
      await axios.post(urlState + '/api/city/create/' + value)
          .then(response => {
            state.commit('allInfo', response.data)
          })
          .catch(error => {
            console.log(error)
          })

        await axios.post(urlState + '/api/city/create-list/' + value)
            .then(response => {
                state.commit('allList', response.data)
            })
            .catch(error => {
                console.log(error)
            })
    },

  }
})
